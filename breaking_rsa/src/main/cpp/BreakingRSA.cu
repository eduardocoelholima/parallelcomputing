/**
 * sequential version of application to find all possible (0 to 3) m
 * that solves m = c^1/3 mod n
 * given c and n
 **
 * @author  Eduardo CoelhoDeLima
 * @version Nov-06-2019
 */

#include <iostream>
#include <math.h>

//CUDA Device code
//this is the kernel that calculates the value of
//(m^3 mod n) and checks if it matches c
__global__ void CalculateM(long long int n, long long int c, int * found, long long int * solutions)
{
    //controls indices for each thread to have a different m to calculate
    long long int current = 0;
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;
    for (int m = index; m <= n-1; m += stride) {
        current = ( ( ((m%n) * (m%n)) % n ) * (m%n)) % n;
        if (current==c) {
            atomicAdd(found, 1); //atomically adds found, which stores how many solutions was found so far
            solutions[found[0]-1] = m; //stores the solution
        }
    }
}

using namespace std;

//since the solutions may no be found in order, sorts the solutions my values of m
void sort(int found, long long int *solutions) {
    long long int tmp;
    for (int i=0;i<found-1;i++){
        if (solutions[i]>solutions[i+1]) {
            tmp = solutions[i+1];
            solutions[i+1] = solutions[i];
            solutions[i] = tmp;
        }
    }
}

//Host code
int main(int argc, char* argv[])
{
    //Check for the right number of parameters (should be exactly 2)
    if (argc != 3) {
        cerr << "Usage: " << argv[0] << " c n" << std::endl;
        return 1;
    }
    else {
        long long int c = std::stoi(argv[1]);
        long long int n = std::stoi(argv[2]);
        int blockSize = 1024;
        int numBlocks = (n + blockSize - 1) / blockSize;
        long long int *solutions;
        int *found;

        //Allocate Unified Memory – accessible from CPU or GPU
        cudaMallocManaged(&solutions, 3*sizeof(long long int));
        cudaMallocManaged(&found, 1*sizeof(int));

        //Initializes number of solutions found
        found[0] = 0;

        //CUDA Kernel Call
        CalculateM<<<numBlocks, blockSize>>>(n, c, found, solutions);

        //Wait for GPU to finish before accessing data on host
        cudaDeviceSynchronize();

        if (found[0]==0) {
            cout << "No cube roots of " << to_string(c) << " (mod " << to_string(n) << ")" << endl;
        }
        else
        {
            sort(found[0], solutions); //sorts solutions by m
            for (int i=0;i<found[0];i++){
                cout << to_string(solutions[i]) << "^3 = " << to_string(c) << " (mod "<< to_string(n) << ")" << endl; //prints solutions
            }
        }

        return 0;
    }
}