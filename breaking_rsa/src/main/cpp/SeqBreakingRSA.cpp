/**
 * sequential version of application to find all possible (0 to 3) m
 * that solves m = c^1/3 mod n
 * given c and n
 **
 * @author  Eduardo CoelhoDeLima
 * @version Nov-06-2019
 */


#include <iostream>
#include <math.h>

using namespace std;

int main(int argc, char* argv[])
{
    // Check for the right number of parameters (should be exactly 2)
    if (argc != 3) {
        cerr << "Usage: " << argv[0] << " c n" << std::endl;
        return 1;
    }
    else {
        //takes 1st and 2nd command-line arguments as c and n
        long long int c = std::stoi(argv[1]);
        long long int n = std::stoi(argv[2]);
        long long int candidate;
        int found = 0;

        //for 0<=m<=n-1, calculates candidates to check which values of m solves
        //m = c^1/3 mod n
        for (int m=0; m<=n-1; m++) {
            candidate = ( ( ((m%n) * (m%n)) % n ) * (m%n)) % n;
            if (candidate==c) {
                cout << to_string(m) << "^3 = " << to_string(c) << " (mod "<< to_string(n) << ")" << endl;
                found++;
            }
            if (found == 3) {
                return 0;
            }
        }

        //if no m is found
        if (found==0) {
            cout << "No cube roots of " << to_string(c) << " (mod " << to_string(n) << ")" << endl;
        }

        return 0;
    }
}