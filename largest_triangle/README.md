# Largest Triangle
Student: Eduardo CoelhoDeLima

## Running Instructions

Compiled classes are available in ./target/classes/edu/rit/cs/

- LargeTriangle.class is the serial version
- LargeTriangleBruteforce.class is a not-optimized serial version that goes over all possible combinations of 3 points in every order possible.
- LargeTriangleMPI.class is the parallel version (using OpenMPI)

### Serial version
As can be seen in the example below, the program takes 3 required arguments: number of points, size of the side (used in the random point generator) and a seed (also for the generator). To run the serial classes, please use the following command on UNIX/MacOS:
```
# cd target/classes
# java edu/rit/cs/LargeTriangle 100 100 142857
```

### Parallel version
This is only required if you want to compile. Make sure all class files are in the same directories in every node. From a node, using the following to compile:
```
# cd src/main/java
# mpijavac -cp /csci654Tools/mpi.jar edu/rit/cs/LargeTriangleMPI.java edu/rit/cs/Point.java edu/rit/cs/RandomPoints.java edu/rit/cs/PointSpec.java 
```

To run the parallel version, you will need at least one node with OpenMPI libraries. If you want to allow more than one node, use the argument --hostname followed by a text file name. You will need a hostfile containing each participating node's name or address (one per line). To specify the number of tasks to be distributed among nodes, use the argument -np followed by the number of tasks. As the sequential version, the program itself takes 3 required arguments: number of points, size of the side (used in the random point generator) and a seed (also for the generator). From a node, using the following to run:
```
# cd target/classes
# mpiexec --allow-run-as-root --hostfile hosts -np 8 java -cp /csci654Tools/mpi.jar edu.rit.cs.LargeTriangleMPI 100 100 123
```



##Project Requirements
<p>Given a group of two-dimensional points, we want to find the&nbsp;<strong>largest triangle,</strong>&nbsp;namely three distinct points that are the vertices of a triangle with the largest area. The area of a triangle is (<em>s</em>(<em>s</em>&nbsp;&minus;&nbsp;<em>a</em>)(<em>s</em>&nbsp;&minus;&nbsp;<em>b</em>)(<em>s</em>&nbsp;&minus;&nbsp;<em>c</em>))<sup>1/2</sup>, where&nbsp;<em>a</em>,&nbsp;<em>b</em>, and&nbsp;<em>c</em>&nbsp;are the lengths of the triangle's sides and&nbsp;<em>s</em>&nbsp;=&nbsp;(<em>a</em>&nbsp;+&nbsp;<em>b</em>&nbsp;+&nbsp;<em>c</em>)/2. The length of a triangle's side is the Euclidean distance between the side's two endpoints.</p>
<p>You will write sequential and cluster parallel versions of a program that finds the largest triangle in a group of points. The program's command line argument is a&nbsp;<em>constructor expression</em>&nbsp;for a PointSpec object from which the program obtains the points' (<em>x,y</em>) coordinates. </p>