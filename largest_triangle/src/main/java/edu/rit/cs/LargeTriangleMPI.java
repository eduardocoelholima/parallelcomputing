package edu.rit.cs;

/**
 * sequential version of application to find the largest
 * triangle area, given the number of points, the side
 * and the random seed
 **
 * @author  Eduardo CoelhoDeLima
 * @version Oct-08-2019
 */

import jdk.nashorn.internal.ir.RuntimeNode;
import mpi.*;
import java.nio.*;

public class LargeTriangleMPI
{

    /**
     * checks if the inputs are valid
     * @param args command line arguments
     * @return boolean
     */
    public static boolean inputsAreValid(String[] args) {

        if (args.length!=3) {
            System.out.println("Must provide 3 command-line arguments for NumOfPoints, Side and Seed");
            return false;
        }

        return true;
    }

    /**
     * calculates the area of the triangle defined by given points
     * @param x1, y1, x2, y2, x3, y3 coordinates (x,y) of given points
     * @return double
     */
    public static double area(double x1, double y1, double x2, double y2, double x3, double y3) {
        double a = Math.sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );
        double b = Math.sqrt( (x3-x2)*(x3-x2) + (y3-y2)*(y3-y2) );
        double c = Math.sqrt( (x1-x3)*(x1-x3) + (y1-y3)*(y1-y3) );
        double s = (a + b + c) / 2;
        return Math.sqrt( (s - a) * (s - b) * (s - c) * s);
    }

    public static void main( String[] args ) throws MPIException {

        //used to calculate program total elapsed time
        long startTime = System.nanoTime();

        //begin of parallel code
        MPI.Init(args);

        //stores current rank and hostname
        int rank = MPI.COMM_WORLD.getRank(), size = MPI.COMM_WORLD.getSize();
        String hostname = MPI.getProcessorName();

        //if ranker output is wanted just uncomment next line
        //System.out.println("Workers="+size+", My rank="+rank+", Running on "+hostname+"\n");

        //checks if the inputs are valid
        if (inputsAreValid(args)) {

            //parses the inputs: numPoints, side and seed
            int numPoints = Integer.parseInt(args[0]);
            int side = Integer.parseInt(args[1]);
            int seed = Integer.parseInt(args[2]);

            //generates numPoints for p1
            RandomPoints rndPoints1 = new RandomPoints(numPoints, side, seed);

            //initialize points p1, p2, p3
            Point p1 = new Point(0,0);
            Point p2 = new Point(0,0);
            Point p3 = new Point(0,0);

            //initialize largest_area, best_p1, best_p2, best_p3
            double largest_area = 0, current_a;
            int best_p1_i=0, best_p2_i=0, best_p3_i=0;
            Point best_p1 = new Point(0,0);
            Point best_p2 = new Point(0,0);
            Point best_p3 = new Point(0,0);

            //loops over possible p1 coordinates
            for (int idx1=(rank+1); idx1<=numPoints; idx1+=size) {

                //sets the offset for the first run of the loop
                if (idx1==(rank+1)) {
                    for (int i=0; i<=rank; i++){

                        if (rndPoints1.hasNext()) p1 = rndPoints1.next();
                        //System.out.println(rank+" idx1="+idx1+" i="+i+" x="+p1.getX()+" y="+p1.getY());
                    }
                }
                else {
                    for (int i=0; i<size; i++) {
                        //System.out.println(rank+" idx1="+idx1+" i="+i+" x="+p1.getX()+" y="+p1.getY());
                        if (rndPoints1.hasNext()) p1 = rndPoints1.next();
                    }
                }


                //generates numPoints for p2
                RandomPoints rndPoints2 = new RandomPoints(numPoints, side, seed);

                //updates the last random point 2 to be equal to idx1
                //this ensures that the next time a number is retrieved for idx2,
                //it will be the index next to the last used idx1
                //thus not visiting same points again for previously calculated triangle areas
                for (int idx2=1;idx2<=idx1; idx2++) {
                    p2 = rndPoints2.next();
                }

                //loops over possible p2 coordinates
                //for (int idx2=1; idx2<=numPoints; idx2++) {
                for (int idx2=idx1+1; idx2<=numPoints; idx2++) {

                    if (rndPoints2.hasNext()) {
                        p2 = rndPoints2.next();
                    }

                    //generates numPoints for p3
                    RandomPoints rndPoints3 = new RandomPoints(numPoints, side, seed);

                    //updates the last random point 3 to be equal to idx2
                    //this ensures that the next time a number is retrieved for idx3,
                    //it will be the index next to the last used idx2
                    //thus not visiting same points again for previously calculated triangle areas
                    for (int idx3=1;idx3<=idx2; idx3++) {
                            p3 = rndPoints3.next();
                    }

                    //loops over possible still-not-visited p3 coordinates
                    for (int idx3=idx2+1; idx3<=numPoints; idx3++) {
                        if (rndPoints3.hasNext()) {
                            p3 = rndPoints3.next();

                            //calculates the area for current p1, p2, p3
                            current_a = area(p1.getX(),p1.getY(),p2.getX(),p2.getY(),p3.getX(),p3.getY());

                            //checks whether current area is greater than the largest one found so far
                            //
                            //notice that in case of a match we will prefer the previously found solution
                            //since per project requirements the solution with smallest 1st index, then 2nd index,
                            //then finally 3rd index will be preferred
                            if (current_a>largest_area) {
                                largest_area = current_a;
                                best_p1_i = idx1;
                                best_p2_i = idx2;
                                best_p3_i = idx3;
                                best_p1 = p1;
                                best_p2 = p2;
                                best_p3 = p3;

                                //Uncomment next line to output every time a new largest area is found
                                //System.out.println(rank+" "+current_a+" "+idx1+" "+idx2+" "+idx3);
                            }
                        }
                    }
                }
            }


            //stores the worker's candidate largest_area
            double candidate_area[] = new double[1], best_area[] = new double[1];
            candidate_area[0] = largest_area;
            int[] candidate_points = new int[10], best_points = new int[10];
            int root = 0;

            //all workes should sync to proceed
            MPI.COMM_WORLD.barrier();

            //find the largest area among workers' candidates
            MPI.COMM_WORLD.allReduce(candidate_area, best_area, 1, MPI.DOUBLE, MPI.MAX);

            //all workes should sync to proceed
            MPI.COMM_WORLD.barrier();

            //only the worker/rank that has the best solution executes the following
            if (Math.abs(best_area[0]-candidate_area[0])<0.001) {
                //outputs the solution found
                System.out.printf("%d %.5g %.5g%n%d %.5g %.5g%n%d %.5g %.5g%n%.5g%n", best_p1_i, best_p1.getX(), best_p1.getY(), best_p2_i, best_p2.getX(), best_p2.getY(), best_p3_i, best_p3.getX(), best_p3.getY(), largest_area);

                //outputs elapsed time
                long endTime = System.nanoTime();
                long totalTime = endTime - startTime;
                System.out.println(totalTime / 1000000 + "ms elapsed");
            }

            //end of parallel execution
            MPI.Finalize();

        }
    }
}
