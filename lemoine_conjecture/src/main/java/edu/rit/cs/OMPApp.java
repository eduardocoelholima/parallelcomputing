package edu.rit.cs;

import java.util.ArrayList;

import static java.lang.Boolean.FALSE;

/**
 * sequential version of application to find the primes p, q
 * that satisfies the lemoine's conjecture that every prime
 * greater than 5 can be expressed as p+2*q
 **
 * @author  Eduardo CoelhoDeLima
 * @version Sep-16-2019
 */

public class OMPApp
{

    /**
     * checks if an integer is odd
     * @param n integer
     * @return boolear
     */
    public static boolean isOdd(int n) {
        if (n%2 == 1) return true;
        else return false;
    }

    /**
     * generate an array of number from range [l,h]
     * @param l integer
     * @param h integer
     * @return arraylist of integers
     */
    public static ArrayList<Integer> generateArray(int l, int h) {

        //check if both l and h are odd numbers
        if (!(isOdd(l)&&isOdd(h))) System.out.println("2 odd numbers are required");

        //makes sure that l<=h
        if (l>h) {
            int temp = l;
            l = h;
            h = temp;
        }

        ArrayList<Integer> range = new ArrayList<Integer>();
        for (int x=l; x<=h; x++) {
            range.add(x);
        }
        return range;
    }

    /**
     * checks if the inputs i,j are valid
     * @param i integer
     * @param j integer
     * @return boolean
     */
    public static boolean inputsAreValid(String[] args) {

        if (args.length!=2) {
            System.out.println("Must provide 2 command-line arguments for lower an upper bounds");
            return false;
        }

        int l = Integer.parseInt(args[0]);
        int h = Integer.parseInt(args[1]);

        //l (lower bound) must >=5 and odd
        if ((l < 5) || !isOdd(l)) {
            System.out.println("Lower bound must be an odd equal or greater than 5");
            return false;
        }

        //h must be >=l and odd
        if ((h < l) || !isOdd(h)) {
            System.out.println("Upper bound must an odd equal or greater than the lower bound");
            return false;
        }

        return true;
    }

    /**
     * outputs the lemoine decomposition for n = p + 2*q
     * p must be odd and prime
     * q must be prime
     * @param n
     */
    public static int[] lemoine(int n, boolean verbose) {

        int p = 3;
        int q = 0;
        boolean halt = false;
        int[] pair = new int[2];

        for (Prime.Iterator pIterator = new Prime.Iterator(); (halt==FALSE); ) {

            p = pIterator.next();
            //if (verbose) System.out.print("p="+p+" ");

            int q_r = (n - p)%2;
            q = (n-p)/2;

            if ( (q_r==0) && (Prime.isPrime(q) || q==2) ) {
                if (verbose) System.out.println(n+" = "+p+" + 2*"+q);
                pair[0] = p;
                pair[1] = q;
                return pair;
            }

            if (p>=n) break; //only called if p is not found
        }

        System.out.println("NOT FOUND. Conjecture Flawed OR n is NOT A PRIME OR n is not greater than 5.");
        return pair;
    }

    public static void main( String[] args )
    {
        long startTime = System.nanoTime();

        //checks if the inputs are valid, otherwise quits
        if (!(inputsAreValid(args))) {
            return;
        }

        int l = Integer.parseInt(args[0]); //lower bound
        int h = Integer.parseInt(args[1]); //upper bound
        int[] pair = new int[2]; //found p, q
        int[] best = new int[3]; //best p, q, n
        best[0] = 0;
        boolean verbose = FALSE;

        // omp parallel for
        for (int n=l; n<=h; n+=2) {

            if (isOdd(n)) {
                pair = lemoine(n, verbose);
            }
            if (pair[0] >= best[0]) {
                best[0] = pair[0];
                best[1] = pair[1];
                best[2] = n;
            }
        }

        System.out.println(best[2]+" = "+best[0]+" + 2*"+best[1]);

        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println(totalTime/1000000+"ms elapsed");

    }
}
