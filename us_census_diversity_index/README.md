# USCB Diversity Index
Calculates US Census Diversity Index per U.S. Counties.

Student: Eduardo CoelhoDeLima


## Running locally with IntelliJ
Before running/compiling, make sure the working directory is set to
```
./us_census_diversity_index
```
To run the code, just locate the following directory and hit RUN
```
./us_census_diversity_index/src/main/java/edu/rit/cs/
```

The census dataset input file is hardcoded as allowed by project requirements, and should be renamed to "censusdata.csv" and placed in subdirectory "dataset".
```
dataset/censusdata.csv
```



## Project Requirements
<p><a name="overview"></a></p>
<p><a name="overview"></a></p>
<p>Write a map-reduce parallel program using <a href="www.spark.org" target="new">Apache Spark</a>. Run the program on a cluster parallel computer to learn about big data and map-reduce parallel programming.</p>
<p><strong>Help with your project:</strong>&nbsp;I am willing to help you with the design of your project in office hours or on Slack channel. If the code isn't working, please describe the problem and post some error messages so that I can help. If we cannot resolve the issues, you will need to visit me during the office hours or make an appointment.</p>
<p></p>
<hr />
<p><a name="system"></a></p>
<h2>U.S. Census Diversity Index</h2>
<p><a name="system"></a></p>
<p>The United States Census Bureau (USCB) estimates the number of persons in each county in each state, categorized by age, gender, race, and other factors. USCB uses six racial categories: White; Black or African American; American Indian or Alaska Native; Asian; Native Hawaiian or Other Pacific Islander; Two or more races.</p>
<p>For example, here are the USCB's population estimates for Monroe County, New York, as of July 1, 2017:</p>
<p></p>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td align="left" valign="top">White</td>
<td width="40"></td>
<td align="right" valign="top">573928</td>
</tr>
<tr>
<td align="left" valign="top">Black or African American</td>
<td width="40"></td>
<td align="right" valign="top">121423</td>
</tr>
<tr>
<td align="left" valign="top">American Indian or Alaska Native</td>
<td width="40"></td>
<td align="right" valign="top">3074</td>
</tr>
<tr>
<td align="left" valign="top">Asian</td>
<td width="40"></td>
<td align="right" valign="top">29053</td>
</tr>
<tr>
<td align="left" valign="top">Native Hawaiian or Other Pacific Islander</td>
<td width="40"></td>
<td align="right" valign="top">517</td>
</tr>
<tr>
<td align="left" valign="top">Two or more races</td>
<td width="40"></td>
<td align="right" valign="top">19667</td>
</tr>
<tr>
<td align="left" valign="top">Total</td>
<td width="40"></td>
<td align="right" valign="top">747662</td>
</tr>
</tbody>
</table>
<p></p>
<p>The&nbsp;<strong>diversity index</strong>&nbsp;<em>D</em>&nbsp;for a population is the probability that two randomly chosen individuals in that population will be of different races. The diversity index is calculated with this formula, where&nbsp;<em>N</em><sub><em>i</em></sub>&nbsp;is the number of individuals in racial category&nbsp;<em>i</em>&nbsp;and&nbsp;<em>T</em>&nbsp;is the total number of individuals:</p>
<p></p>
<center>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td align="right" valign="center"></td>
<td align="center" valign="center">6</td>
<td align="left" valign="center"></td>
</tr>
<tr>
<td align="right" valign="center"><em>D</em>&nbsp;&nbsp;=&nbsp;&nbsp;1/<em>T</em><sup>2</sup>&nbsp;</td>
<td align="center" valign="center">&Sigma;</td>
<td align="left" valign="center">&nbsp;<em>N</em><sub><em>i</em></sub>&nbsp;(<em>T</em>&nbsp;&minus;&nbsp;<em>N</em><sub><em>i</em></sub>)</td>
</tr>
<tr>
<td align="left" valign="center"></td>
<td align="center" valign="center"><em>i</em>=1</td>
<td align="right" valign="center"></td>
</tr>
</tbody>
</table>
</center>
<p>For example, the diversity index of Monroe County, New York as of July 1, 2017 is 0.38216. The diversity index should be calculated using double precision floating point (type&nbsp;<tt>double</tt>).</p>
<p>I have downloaded a&nbsp;<a href="https://www2.census.gov/programs-surveys/popest/datasets/2010-2017/counties/asrh/cc-est2017-alldata.csv">census dataset</a>&nbsp;from the USCB&nbsp;<a href="https://www.census.gov/data/tables/2017/demo/popest/counties-detail.html">web site</a>. The dataset contains information for every county in every state from 2010 to 2017. I installed the full dataset on the&nbsp;<tt>tardis</tt>&nbsp;machine in a file named&nbsp;<tt>/var/tmp/USCensus/cc-est2017-alldata.csv</tt>. You can log into&nbsp;<tt>tardis</tt>&nbsp;and look at the dataset. I also partitioned the dataset and installed the partitions on the local hard disks of the&nbsp;<tt>dr00</tt>&nbsp;through&nbsp;<tt>dr09</tt>&nbsp;nodes of the&nbsp;<tt>tardis</tt>&nbsp;cluster. Each node has one file named&nbsp;<tt>/var/tmp/USCensus/cc-est2017-alldata.csv</tt>. Each file totals about 14 megabytes, and the whole dataset totals about 140 megabytes.</p>
<p>Each record in the dataset (each line in the file) consists of several fields. Each field is separated from the next field by a comma. The fields contain the following information. Fields are indexed from left to right starting at 0. Only the fields needed for this project are listed.</p>
<p></p>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td align="left" valign="top"><em>Index</em></td>
<td width="20"></td>
<td align="left" valign="top"><em>Name</em></td>
<td width="20"></td>
<td align="left" valign="top"><em>Contents</em></td>
<td width="20"></td>
<td align="left" valign="top"><em>Type</em></td>
</tr>
<tr>
<td align="left" valign="top">3</td>
<td width="20"></td>
<td align="left" valign="top">STNAME</td>
<td width="20"></td>
<td align="left" valign="top">State name</td>
<td width="20"></td>
<td align="left" valign="top">String</td>
</tr>
<tr>
<td align="left" valign="top">4</td>
<td width="20"></td>
<td align="left" valign="top">CTYNAME</td>
<td width="20"></td>
<td align="left" valign="top">County name</td>
<td width="20"></td>
<td align="left" valign="top">String</td>
</tr>
<tr>
<td align="left" valign="top">5</td>
<td width="20"></td>
<td align="left" valign="top">YEAR</td>
<td width="20"></td>
<td align="left" valign="top">Year</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">6</td>
<td width="20"></td>
<td align="left" valign="top">AGEGRP</td>
<td width="20"></td>
<td align="left" valign="top">Age group</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">10</td>
<td width="20"></td>
<td align="left" valign="top">WA_MALE</td>
<td width="20"></td>
<td align="left" valign="top">White male population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">11</td>
<td width="20"></td>
<td align="left" valign="top">WA_FEMALE</td>
<td width="20"></td>
<td align="left" valign="top">White female population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">12</td>
<td width="20"></td>
<td align="left" valign="top">BA_MALE</td>
<td width="20"></td>
<td align="left" valign="top">Black or African American male population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">13</td>
<td width="20"></td>
<td align="left" valign="top">BA_FEMALE</td>
<td width="20"></td>
<td align="left" valign="top">Black or African American female population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">14</td>
<td width="20"></td>
<td align="left" valign="top">IA_MALE</td>
<td width="20"></td>
<td align="left" valign="top">American Indian or Alaska Native male population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">15</td>
<td width="20"></td>
<td align="left" valign="top">IA_FEMALE</td>
<td width="20"></td>
<td align="left" valign="top">American Indian or Alaska Native female population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">16</td>
<td width="20"></td>
<td align="left" valign="top">AA_MALE</td>
<td width="20"></td>
<td align="left" valign="top">Asian male population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">17</td>
<td width="20"></td>
<td align="left" valign="top">AA_FEMALE</td>
<td width="20"></td>
<td align="left" valign="top">Asian female population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">18</td>
<td width="20"></td>
<td align="left" valign="top">NA_MALE</td>
<td width="20"></td>
<td align="left" valign="top">Native Hawaiian or Other Pacific Islander male population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">19</td>
<td width="20"></td>
<td align="left" valign="top">NA_FEMALE</td>
<td width="20"></td>
<td align="left" valign="top">Native Hawaiian or Other Pacific Islander female population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">20</td>
<td width="20"></td>
<td align="left" valign="top">TOM_MALE</td>
<td width="20"></td>
<td align="left" valign="top">Two or more races male population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
<tr>
<td align="left" valign="top">21</td>
<td width="20"></td>
<td align="left" valign="top">TOM_FEMALE</td>
<td width="20"></td>
<td align="left" valign="top">Two or more races female population</td>
<td width="20"></td>
<td align="left" valign="top">Integer</td>
</tr>
</tbody>
</table>
<p></p>
<p>The YEAR field is an integer giving the record's year:&nbsp;<br />1 = April 1, 2010 census population&nbsp;<br />2 = April 1, 2010 population estimates base&nbsp;<br />3 = July 1, 2010 population estimate&nbsp;<br />4 = July 1, 2011 population estimate&nbsp;<br />5 = July 1, 2012 population estimate&nbsp;<br />6 = July 1, 2013 population estimate&nbsp;<br />7 = July 1, 2014 population estimate&nbsp;<br />8 = July 1, 2015 population estimate&nbsp;<br />9 = July 1, 2016 population estimate&nbsp;<br />10 = July 1, 2017 population estimate</p>
<p>The AGEGRP field is an integer giving the record's age group. We are only interested in records with AGEGRP = 0; such records contain the total population for all age groups.</p>
<p>The number of individuals in a racial category is the sum of the number of males and the number of females. For example, the number of white individuals is WA_MALE + WA_FEMALE.</p>
<p>The USCB&nbsp;<a href="https://www.census.gov/data/tables/2017/demo/popest/counties-detail.html">web site</a>&nbsp;has a&nbsp;<a href="https://www2.census.gov/programs-surveys/popest/technical-documentation/file-layouts/2010-2017/cc-est2017-alldata.pdf">document</a>&nbsp;that describes the census dataset in full detail.</p>
<p>You will write a Spark MapReduce job to analyze the census data and calculate the diversity index for every county in a given state or states for a given year. The program will also calculate the diversity index for the overall population of each state.</p>
<p></p>
<hr />
<p><a name="inputoutput"></a></p>
<h2>Program Input and Output</h2>
<p><a name="inputoutput"></a></p>
<p>Your program should take census data file as input (either in the program or a command line argument).</p>
<!--<p>The number of mapper threads per mapper task is also specified using the&nbsp;<tt>pj2</tt>&nbsp;program's&nbsp;<tt>"threads="</tt>&nbsp;option.</p>-->
<p>The census data analysis program's output consists of a series of sections. Each section reports results for one state. The sections appear in ascending order of the state name.</p>
<pre></pre>
<p>The output should look like the following</p>
<!--
<p>A Spark job is a cluster parallel program. This means that, when running on the&nbsp;<tt>tardis</tt>&nbsp;cluster, you must include the&nbsp;<tt>"jar="</tt>&nbsp;option on the&nbsp;<tt>pj2</tt>&nbsp;command line. The JAR file must contain all the Java class files (<tt>.class</tt>&nbsp;files) in your project.</p>
<p>Here is an example of the analysis program running on the&nbsp;<tt>tardis</tt>&nbsp;cluster. (<em>Note:</em>&nbsp;The lines reporting job statistics were printed by&nbsp;<tt>pj2</tt>; they are not part of the analysis program's output.)</p>-->
<pre>[Alabama,Autauga County,0.3509529354189893]<br />[Alabama,Baldwin County,0.22703415227240026]<br />[Alabama,Barbour County,0.5201912665001125]<br />[Alabama,Bibb County,0.36474615468663946]<br />[Alabama,Blount County,0.07275405231720622]<br />[Alabama,Bullock County,0.42975603086821046]<br />[Alabama,Butler County,0.5164475024792679]<br />[Alabama,Calhoun County,0.3779498849796942]<br />[Alabama,Chambers County,0.5032526628314044]<br />[Alabama,Cherokee County,0.1289766197336407]<br />[Alabama,Chilton County,0.22194564663854674]<br />[Alabama,Choctaw County,0.49859290783471816]<br />[Alabama,Clarke County,0.5102750189381539]<br />[Alabama,Clay County,0.28905293246229746]<br />[Alabama,Cleburne County,0.09751152846969612]<br />[Alabama,Coffee County,0.37945931313725734]<br />[Alabama,Colbert County,0.3171453058986595]<br />[Alabama,Conecuh County,0.5162908370411342]<br />[Alabama,Coosa County,0.4580730840773218]<br />[Alabama,Covington County,0.2628926814436877]<br />[Alabama,Crenshaw County,0.4140092937638725]<br />[Alabama,Cullman County,0.06807344761561963]<br />[Alabama,Dale County,0.39252877892694676]<br />[Alabama,Dallas County,0.42895326066377165]<br />[Alabama,DeKalb County,0.1370742959919811]<br />[Alabama,Elmore County,0.3710866733705336]<br />[Alabama,Escambia County,0.5059790116462756]<br />[Alabama,Etowah County,0.31186852698006357]<br />[Alabama,Fayette County,0.2344508467980336]<br />...</pre>
<p></p>
<p>Full output file can be found <a href="https://gitlab.com/SpiRITlab/parallelcomputing/blob/master/us_census_diversity_index/dataset/USCB-output.txt" target="_blank" rel="noopener">here</a>.</p>
<p></p>
<hr />
<p><a name="softwarereqts"></a></p>
<h2>Software Requirements</h2>
<p><a name="softwarereqts"></a></p>
<ol type="1">
<li>The program must be run within Apache Spark. For this project, local runs with 4 and 8 threads are fine.</li>
</ol>
<p></p>
<hr />
<p><a name="softwaredesign"></a></p>
<h2>Software Design Criteria</h2>
<p><a name="softwaredesign"></a></p>
<p></p>
<ol type="1">
<li>The program must be a map-reduce job using the Spark framework.
<p></p>
</li>
<li>The program must be designed using object oriented design principles as appropriate.
<p></p>
</li>
<li>The program must make use of reusable software components as appropriate.
<p></p>
</li>
<li>Each class or interface must include a Javadoc comment describing the overall class or interface.
<p></p>
</li>
<li>Each constructor and method within each class or interface must include a Javadoc comment describing the overall constructor or method, the arguments if any, the return value if any, and the exceptions thrown if any.
<p><em>Note:</em>&nbsp;See my Java source files which we studied in class for the style of Javadoc comments I'm looking for.</p>
<p><em>Note: If your program's design does not conform to Software Design Criteria 1 through 5,&nbsp;<strong>you will lose credit</strong>&nbsp;on your project.</em>&nbsp;See the&nbsp;<a href="#grading">Grading Criteria</a>&nbsp;below.</p>
</li>
</ol>
<p></p>
<hr />
<p><a name="submissionreqts"></a></p>
<h2>Submission Requirements</h2>
<p><a name="submissionreqts"></a></p>
<p>Project submission is done through version control system gitlab.com. You will need to create a private gitlab repository for each project. Please add me (username: <strong>peiworld</strong>) as a developer.</p>
<p>Check course schedule for the submission deadline. The date/time at which your last commit will determine whether your project meets the deadline.</p>
<p><strong><em>I STRONGLY advise you to start earlier and keep a steady progress because 10% of the project points are graded based on your project progress.</em></strong></p>
<p></p>
<hr />
<p><a name="grading"></a></p>
<h2>Grading Criteria</h2>
<p><a name="grading"></a></p>
<p>I will grade your project by:</p>
<p></p>
<ul>
<li>
<ul>
<li>All of the&nbsp;<a href="#softwaredesign">Software Design Criteria</a>&nbsp;are fully met.</li>
<li>Some of the&nbsp;<a href="#softwaredesign">Software Design Criteria</a>&nbsp;are not fully met.Evaluating the design of your program, as documented in the Javadoc and as implemented in the source code.</li>
</ul>
</li>
<li>Running your map-reduce parallel program. "Correct output" means "output fulfills&nbsp;<em>all</em>&nbsp;the&nbsp;<a href="#softwarereqts">Software Requirements</a><em>exactly.</em>"
<p></p>
</li>
</ul>
<p></p>
<p>For further information, see the&nbsp;<a href="https://www.cs.rit.edu/~ark/654/policies.shtml">Course Grading and Policies</a>&nbsp;.</p>
<p><a name="testcases"></a></p>
<!--
<p><strong><em>IMPORTANT NOTE</em></strong>&nbsp;<br />Running on the&nbsp;<tt>tardis</tt>&nbsp;cluster, each test case 4&ndash;20 should have taken about 4 seconds. I set a 20-second timeout on each test case. If the program timed out, it indicates a bad design.</p>
<p><strong><em>IMPORTANT NOTE</em></strong>&nbsp;<br />I have accurately recorded in your grade file what I observed during my testing. If you run the test cases and do not get the same results as I, then there is a flaw in your design somewhere that causes inconsistent results (perhaps due to incorrect parallelization), and I am not going to change your grade for the original submission. You may still&nbsp;<a href="#resubmission">resubmit</a>&nbsp;your project.</p>
<pre>1.  java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv
2.  java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv one "Delaware"
3.  java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 1 "Delaware" "Delaware"
4.  java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 1 "Alabama"
5.  java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 2 "Arkansas"
6.  java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 3 "Arizona"
7.  java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 4 "Arizona"
8.  java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 5 "Colorado" "California"
9.  java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 6 "Delaware" "Connecticut"
10. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 7 "Florida" "Georgia"
11. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 8 "Idaho" "Hawaii"
12. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 9 "Illinois" "Indiana"
13. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 10 "Iowa" "Kansas"
14. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 1 "Kentucky" "Louisiana"
15. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 2 "Massachusetts" "Maryland" "Maine"
16. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 3 "New Mexico" "New Jersey" "New Hampshire"
17. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 4 "New York" "North Carolina" "North Dakota"
18. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 5 "Ohio" "Pennsylvania" "Oregon"
19. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 6 "Rhode Island" "South Dakota" "Tennessee"
20. java pj2 debug=none timelimit=20 jar=p4.jar threads=2 DivIndex dr00,dr01,dr02,dr03,dr04,dr05,dr06,dr07,dr08,dr09 /var/tmp/ark/USCensus/cc-est2017-alldata.csv 7
</pre>--><hr />
<p><a name="late"></a></p>
<h2>Late Projects</h2>
<p><a name="late"></a></p>
<p>I will take the last commit version as your project submission. You may request for extension and make further commits. There will be 10% penalty points for each day after the deadline. There is no penalty for an extension for medical reason. You need to provide evidence for such an extension. See the Course Policies for my&nbsp;<a href="https://www.cs.rit.edu/~ark/654/policies.shtml#extensions">policy on extensions</a>.</p>
<p></p>
<hr />
<p><a name="plagiarism"></a></p>
<h2>Plagiarism</h2>
<p><a name="plagiarism"></a></p>
<p>Programming Project 4 must be entirely your own individual work. I will not tolerate plagiarism. If in my judgment the project is not entirely your own work, you will automatically receive, as a minimum, a grade of zero for the assignment. See the Course Policies for my&nbsp;<a href="https://www.cs.rit.edu/~ark/654/policies.shtml#plagiarism">policy on plagiarism</a>.</p>
<p></p>
<hr />
<p><a name="resubmission"></a></p>
<h2>Resubmission</h2>
<p><a name="resubmission"></a></p>
<p>If you so choose, you may revise your project after you have received the grade for the original version. I will grade the revised version using the same criteria as the original version, then I will subtract 10% of the project points as a resubmission penalty. The revised grade will replace the original grade, even if the revised grade is less than the original grade.</p>