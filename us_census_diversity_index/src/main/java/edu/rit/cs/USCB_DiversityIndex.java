/**
 * Spark application to calculate the Diversity Index for all U.S. counties
 * Input is hardcoded as ./dataset/censusdata.csv and should be Official US Census Dataset, which can retrieved from
 * https://www2.census.gov/programs-surveys/popest/datasets/2010-2017/counties/asrh/cc-est2017-alldata.csv
 * Results will be saved to ./dataset/USCB-Outputs subdirectory
 *
 * @author  Eduardo CoelhoDeLima
 * @version Nov-23-2019
 */

package edu.rit.cs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import scala.Tuple2;
import java.io.File;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class USCB_DiversityIndex {

    public static final String OutputDirectory = "dataset/USCB-Outputs";
    public static final String DatasetFile = "dataset/censusdata.csv";

    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null)
            for (File file : allContents)
                deleteDirectory(file);
        return directoryToBeDeleted.delete();
    }
    /**
     * reads file from USCB dataset, calculates Diversity Index for each County
     * @param spark an initiated Spark session
     */
    public static void calculateIndexPerCounty(SparkSession spark) {

        // We used the following columns from the dataset:
        // STATE,COUNTY,STNAME,CTYNAME,YEAR,AGEGRP,TOT_POP,TOT_MALE,TOT_FEMALE,WA_MALE,WA_FEMALE,BA_MALE,BA_FEMALE,
        // IA_MALE,IA_FEMALE,AA_MALE,AA_FEMALE,NA_MALE,NA_FEMALE,TOM_MALE,TOM_FEMALE
        Dataset ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .csv(DatasetFile);
        ds = ds.withColumn("YEAR", ds.col("YEAR").cast(DataTypes.IntegerType))
                .withColumn("AGEGRP", ds.col("AGEGRP").cast(DataTypes.IntegerType))
                .withColumn("STNAME", ds.col("STNAME").cast(DataTypes.StringType))
                .withColumn("CTYNAME", ds.col("CTYNAME").cast(DataTypes.StringType));

        // Instantiate an encoder for our dataset and filters whole dataset for YEAR=10 and AGEGRP=0
        Encoder<USCensus> censusEncoder = Encoders.bean(USCensus.class);
        Dataset<USCensus> ds1 = ds.as(censusEncoder).filter("AGEGRP==0");

        // Sums female and male for each race group
        Dataset<Row> ds2 = ds1.withColumn("WA", ds1.col("WA_MALE").plus(ds1.col("WA_FEMALE")))
            .withColumn("BA", ds1.col("BA_MALE").plus(ds1.col("BA_FEMALE"))
                    .cast(DataTypes.LongType))
            .withColumn("IA", ds1.col("IA_MALE").plus(ds1.col("IA_FEMALE"))
                    .cast(DataTypes.LongType))
            .withColumn("AA", ds1.col("AA_MALE").plus(ds1.col("AA_FEMALE"))
                    .cast(DataTypes.LongType))
            .withColumn("NA", ds1.col("NA_MALE").plus(ds1.col("NA_FEMALE"))
                    .cast(DataTypes.LongType))
            .withColumn("TOM", ds1.col("TOM_MALE").plus(ds1.col("TOM_FEMALE"))
                    .cast(DataTypes.LongType))
            .withColumn("TOT", ds1.col("TOT_MALE").plus(ds1.col("TOT_FEMALE"))
                    .cast(DataTypes.LongType));

        // Sums matching st,cty (due to multiple row entries for each census year)
        ds2 = ds2.groupBy("STNAME", "CTYNAME").sum("TOT", "TOM", "NA", "AA", "IA", "BA", "WA");

        // Calculates intermediary values N_i * (T - N_i), each i is a row in the dataset
        // Long type need to be casted to make sure the operations are within range
        ds2 = ds2.withColumn("iWA", ds2.col("sum(TOT)").cast(DataTypes.LongType)
                .minus(ds2.col("sum(WA)")).multiply(ds2.col("sum(WA)")))
            .withColumn("iBA", ds2.col("sum(TOT)").cast(DataTypes.LongType)
                    .minus(ds2.col("sum(BA)")).multiply(ds2.col("sum(BA)")))
            .withColumn("iIA", ds2.col("sum(TOT)").cast(DataTypes.LongType)
                    .minus(ds2.col("sum(IA)")).multiply(ds2.col("sum(IA)")))
            .withColumn("iAA", ds2.col("sum(TOT)").cast(DataTypes.LongType)
                    .minus(ds2.col("sum(AA)")).multiply(ds2.col("sum(AA)")))
            .withColumn("iNA", ds2.col("sum(TOT)").cast(DataTypes.LongType)
                    .minus(ds2.col("sum(NA)")).multiply(ds2.col("sum(NA)")))
            .withColumn("iTOM", ds2.col("sum(TOT)").cast(DataTypes.LongType)
                    .minus(ds2.col("sum(TOM)")).multiply(ds2.col("sum(TOM)")));

        // Then sums up intermediary values and divide by T^2
        ds2 = ds2.withColumn("i", ds2.col("iWA").cast(DataTypes.LongType)
                .plus(ds2.col("iBA")).plus(ds2.col("iIA")).plus(ds2.col("iAA"))
                .plus(ds2.col("iNA")).plus(ds2.col("iTOM")).cast(DataTypes.DoubleType)
                .divide(ds2.col("sum(TOT)")).divide(ds2.col("sum(TOT)")));

        // Final results
        ds2 = ds2.select(ds2.col("STNAME"), ds2.col("CTYNAME"), ds2.col("i") )
        .sort("STNAME", "CTYNAME");

        // In case we want to print results to stdout
//        ds2.filter("STNAME=='Alabama'").show();
//        ds2.filter("CTYNAME=='Monroe County' AND STNAME=='New York'").show();
//        ds2.toJavaRDD().foreach(r -> { System.out.println(r); } ); // to print all results

        // Saves results to file
        ds2 = ds2.coalesce(1);
        deleteDirectory(new File(OutputDirectory));
        ds2.toJavaRDD().saveAsTextFile(OutputDirectory);

    }


    public static void main(String[] args) throws Exception {
        // Create a SparkConf that loads defaults from system properties and the classpath
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[4]");

        // Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
        sparkConf.setAppName("USCB_DiversityIndex");

        // Creates a session for Spark
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        // Creates Spark context
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        // Index Calculation Spark Routine Call
        calculateIndexPerCounty(spark);

        // Stops Spark context
        jsc.close();

        // Stops Spark session
        spark.close();
    }
}
