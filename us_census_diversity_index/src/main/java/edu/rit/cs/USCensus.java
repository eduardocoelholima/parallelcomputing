package edu.rit.cs;

//SUMLEV,STATE,COUNTY,STNAME,CTYNAME,YEAR,AGEGRP,TOT_POP,TOT_MALE,TOT_FEMALE,WA_MALE,WA_FEMALE,BA_MALE,BA_FEMALE,IA_MALE,IA_FEMALE,AA_MALE,AA_FEMALE,NA_MALE,NA_FEMALE,TOM_MALE,TOM_FEMALE,WAC_MALE,WAC_FEMALE,BAC_MALE,BAC_FEMALE,IAC_MALE,IAC_FEMALE,AAC_MALE,AAC_FEMALE,NAC_MALE,NAC_FEMALE,NH_MALE,NH_FEMALE,NHWA_MALE,NHWA_FEMALE,NHBA_MALE,NHBA_FEMALE,NHIA_MALE,NHIA_FEMALE,NHAA_MALE,NHAA_FEMALE,NHNA_MALE,NHNA_FEMALE,NHTOM_MALE,NHTOM_FEMALE,NHWAC_MALE,NHWAC_FEMALE,NHBAC_MALE,NHBAC_FEMALE,NHIAC_MALE,NHIAC_FEMALE,NHAAC_MALE,NHAAC_FEMALE,NHNAC_MALE,NHNAC_FEMALE,H_MALE,H_FEMALE,HWA_MALE,HWA_FEMALE,HBA_MALE,HBA_FEMALE,HIA_MALE,HIA_FEMALE,HAA_MALE,HAA_FEMALE,HNA_MALE,HNA_FEMALE,HTOM_MALE,HTOM_FEMALE,HWAC_MALE,HWAC_FEMALE,HBAC_MALE,HBAC_FEMALE,HIAC_MALE,HIAC_FEMALE,HAAC_MALE,HAAC_FEMALE,HNAC_MALE,HNAC_FEMALE
public class USCensus {

    private long SUMLEV;
    private long STATE;
    private long COUNTY;
    private String STNAME;
    private String CTYNAME;
    private long YEAR;
    private long AGEGRP;
    private long TOT_POP;
    private long TOT_MALE;
    private long TOT_FEMALE;
    private long WA_MALE;
    private long WA_FEMALE;
    private long BA_MALE;
    private long BA_FEMALE;
    private long IA_MALE;
    private long IA_FEMALE;
    private long AA_MALE;
    private long AA_FEMALE;
    private long NA_MALE;
    private long NA_FEMALE;
    private long TOM_MALE;
    private long TOM_FEMALE;
    private long WAC_MALE;
    private long WAC_FEMALE;
    private long BAC_MALE;
    private long BAC_FEMALE;
    private long IAC_MALE;
    private long IAC_FEMALE;
    private long AAC_MALE;
    private long AAC_FEMALE;
    private long NAC_MALE;
    private long NAC_FEMALE;
    private long NH_MALE;
    private long NH_FEMALE;
    private long NHWA_MALE;
    private long NHWA_FEMALE;
    private long NHBA_MALE;
    private long NHBA_FEMALE;
    private long NHIA_MALE;
    private long NHIA_FEMALE;
    private long NHAA_MALE;
    private long NHAA_FEMALE;
    private long NHNA_MALE;
    private long NHNA_FEMALE;
    private long NHTOM_MALE;
    private long NHTOM_FEMALE;
    private long NHWAC_MALE;
    private long NHWAC_FEMALE;
    private long NHBAC_MALE;
    private long NHBAC_FEMALE;
    private long NHIAC_MALE;
    private long NHIAC_FEMALE;
    private long NHAAC_MALE;
    private long NHAAC_FEMALE;
    private long NHNAC_MALE;
    private long NHNAC_FEMALE;
    private long H_MALE;
    private long H_FEMALE;
    private long HWA_MALE;
    private long HWA_FEMALE;
    private long HBA_MALE;
    private long HBA_FEMALE;
    private long HIA_MALE;
    private long HIA_FEMALE;
    private long HAA_MALE;
    private long HAA_FEMALE;
    private long HNA_MALE;
    private long HNA_FEMALE;
    private long HTOM_MALE;
    private long HTOM_FEMALE;
    private long HWAC_MALE;
    private long HWAC_FEMALE;
    private long HBAC_MALE;
    private long HBAC_FEMALE;
    private long HIAC_MALE;
    private long HIAC_FEMALE;
    private long HAAC_MALE;
    private long HAAC_FEMALE;
    private long HNAC_MALE;
    private long HNAC_FEMALE;

    public long getSUMLEV() {
        return SUMLEV;
    }

    public void setSUMLEV(long SUMLEV) {
        this.SUMLEV = SUMLEV;
    }

    public long getSTATE() {
        return STATE;
    }

    public void setSTATE(long STATE) {
        this.STATE = STATE;
    }

    public long getCOUNTY() {
        return COUNTY;
    }

    public void setCOUNTY(long COUNTY) {
        this.COUNTY = COUNTY;
    }

    public String getSTNAME() {
        return STNAME;
    }

    public void setSTNAME(String STNAME) {
        this.STNAME = STNAME;
    }

    public String getCTYNAME() {
        return CTYNAME;
    }

    public void setCTYNAME(String CTYNAME) {
        this.CTYNAME = CTYNAME;
    }

    public long getYEAR() {
        return YEAR;
    }

    public void setYEAR(long YEAR) {
        this.YEAR = YEAR;
    }

    public long getAGEGRP() {
        return AGEGRP;
    }

    public void setAGEGRP(long AGEGRP) {
        this.AGEGRP = AGEGRP;
    }

    public long getTOT_POP() {
        return TOT_POP;
    }

    public void setTOT_POP(long TOT_POP) {
        this.TOT_POP = TOT_POP;
    }

    public long getTOT_MALE() {
        return TOT_MALE;
    }

    public void setTOT_MALE(long TOT_MALE) {
        this.TOT_MALE = TOT_MALE;
    }

    public long getTOT_FEMALE() {
        return TOT_FEMALE;
    }

    public void setTOT_FEMALE(long TOT_FEMALE) {
        this.TOT_FEMALE = TOT_FEMALE;
    }

    public long getWA_MALE() {
        return WA_MALE;
    }

    public void setWA_MALE(long WA_MALE) {
        this.WA_MALE = WA_MALE;
    }

    public long getWA_FEMALE() {
        return WA_FEMALE;
    }

    public void setWA_FEMALE(long WA_FEMALE) {
        this.WA_FEMALE = WA_FEMALE;
    }

    public long getBA_MALE() {
        return BA_MALE;
    }

    public void setBA_MALE(long BA_MALE) {
        this.BA_MALE = BA_MALE;
    }

    public long getBA_FEMALE() {
        return BA_FEMALE;
    }

    public void setBA_FEMALE(long BA_FEMALE) {
        this.BA_FEMALE = BA_FEMALE;
    }

    public long getIA_MALE() {
        return IA_MALE;
    }

    public void setIA_MALE(long IA_MALE) {
        this.IA_MALE = IA_MALE;
    }

    public long getIA_FEMALE() {
        return IA_FEMALE;
    }

    public void setIA_FEMALE(long IA_FEMALE) {
        this.IA_FEMALE = IA_FEMALE;
    }

    public long getAA_MALE() {
        return AA_MALE;
    }

    public void setAA_MALE(long AA_MALE) {
        this.AA_MALE = AA_MALE;
    }

    public long getAA_FEMALE() {
        return AA_FEMALE;
    }

    public void setAA_FEMALE(long AA_FEMALE) {
        this.AA_FEMALE = AA_FEMALE;
    }

    public long getNA_MALE() {
        return NA_MALE;
    }

    public void setNA_MALE(long NA_MALE) {
        this.NA_MALE = NA_MALE;
    }

    public long getNA_FEMALE() {
        return NA_FEMALE;
    }

    public void setNA_FEMALE(long NA_FEMALE) {
        this.NA_FEMALE = NA_FEMALE;
    }

    public long getTOM_MALE() {
        return TOM_MALE;
    }

    public void setTOM_MALE(long TOM_MALE) {
        this.TOM_MALE = TOM_MALE;
    }

    public long getTOM_FEMALE() {
        return TOM_FEMALE;
    }

    public void setTOM_FEMALE(long TOM_FEMALE) {
        this.TOM_FEMALE = TOM_FEMALE;
    }

    public long getWAC_MALE() {
        return WAC_MALE;
    }

    public void setWAC_MALE(long WAC_MALE) {
        this.WAC_MALE = WAC_MALE;
    }

    public long getWAC_FEMALE() {
        return WAC_FEMALE;
    }

    public void setWAC_FEMALE(long WAC_FEMALE) {
        this.WAC_FEMALE = WAC_FEMALE;
    }

    public long getBAC_MALE() {
        return BAC_MALE;
    }

    public void setBAC_MALE(long BAC_MALE) {
        this.BAC_MALE = BAC_MALE;
    }

    public long getBAC_FEMALE() {
        return BAC_FEMALE;
    }

    public void setBAC_FEMALE(long BAC_FEMALE) {
        this.BAC_FEMALE = BAC_FEMALE;
    }

    public long getIAC_MALE() {
        return IAC_MALE;
    }

    public void setIAC_MALE(long IAC_MALE) {
        this.IAC_MALE = IAC_MALE;
    }

    public long getIAC_FEMALE() {
        return IAC_FEMALE;
    }

    public void setIAC_FEMALE(long IAC_FEMALE) {
        this.IAC_FEMALE = IAC_FEMALE;
    }

    public long getAAC_MALE() {
        return AAC_MALE;
    }

    public void setAAC_MALE(long AAC_MALE) {
        this.AAC_MALE = AAC_MALE;
    }

    public long getAAC_FEMALE() {
        return AAC_FEMALE;
    }

    public void setAAC_FEMALE(long AAC_FEMALE) {
        this.AAC_FEMALE = AAC_FEMALE;
    }

    public long getNAC_MALE() {
        return NAC_MALE;
    }

    public void setNAC_MALE(long NAC_MALE) {
        this.NAC_MALE = NAC_MALE;
    }

    public long getNAC_FEMALE() {
        return NAC_FEMALE;
    }

    public void setNAC_FEMALE(long NAC_FEMALE) {
        this.NAC_FEMALE = NAC_FEMALE;
    }

    public long getNH_MALE() {
        return NH_MALE;
    }

    public void setNH_MALE(long NH_MALE) {
        this.NH_MALE = NH_MALE;
    }

    public long getNH_FEMALE() {
        return NH_FEMALE;
    }

    public void setNH_FEMALE(long NH_FEMALE) {
        this.NH_FEMALE = NH_FEMALE;
    }

    public long getNHWA_MALE() {
        return NHWA_MALE;
    }

    public void setNHWA_MALE(long NHWA_MALE) {
        this.NHWA_MALE = NHWA_MALE;
    }

    public long getNHWA_FEMALE() {
        return NHWA_FEMALE;
    }

    public void setNHWA_FEMALE(long NHWA_FEMALE) {
        this.NHWA_FEMALE = NHWA_FEMALE;
    }

    public long getNHBA_MALE() {
        return NHBA_MALE;
    }

    public void setNHBA_MALE(long NHBA_MALE) {
        this.NHBA_MALE = NHBA_MALE;
    }

    public long getNHBA_FEMALE() {
        return NHBA_FEMALE;
    }

    public void setNHBA_FEMALE(long NHBA_FEMALE) {
        this.NHBA_FEMALE = NHBA_FEMALE;
    }

    public long getNHIA_MALE() {
        return NHIA_MALE;
    }

    public void setNHIA_MALE(long NHIA_MALE) {
        this.NHIA_MALE = NHIA_MALE;
    }

    public long getNHIA_FEMALE() {
        return NHIA_FEMALE;
    }

    public void setNHIA_FEMALE(long NHIA_FEMALE) {
        this.NHIA_FEMALE = NHIA_FEMALE;
    }

    public long getNHAA_MALE() {
        return NHAA_MALE;
    }

    public void setNHAA_MALE(long NHAA_MALE) {
        this.NHAA_MALE = NHAA_MALE;
    }

    public long getNHAA_FEMALE() {
        return NHAA_FEMALE;
    }

    public void setNHAA_FEMALE(long NHAA_FEMALE) {
        this.NHAA_FEMALE = NHAA_FEMALE;
    }

    public long getNHNA_MALE() {
        return NHNA_MALE;
    }

    public void setNHNA_MALE(long NHNA_MALE) {
        this.NHNA_MALE = NHNA_MALE;
    }

    public long getNHNA_FEMALE() {
        return NHNA_FEMALE;
    }

    public void setNHNA_FEMALE(long NHNA_FEMALE) {
        this.NHNA_FEMALE = NHNA_FEMALE;
    }

    public long getNHTOM_MALE() {
        return NHTOM_MALE;
    }

    public void setNHTOM_MALE(long NHTOM_MALE) {
        this.NHTOM_MALE = NHTOM_MALE;
    }

    public long getNHTOM_FEMALE() {
        return NHTOM_FEMALE;
    }

    public void setNHTOM_FEMALE(long NHTOM_FEMALE) {
        this.NHTOM_FEMALE = NHTOM_FEMALE;
    }

    public long getNHWAC_MALE() {
        return NHWAC_MALE;
    }

    public void setNHWAC_MALE(long NHWAC_MALE) {
        this.NHWAC_MALE = NHWAC_MALE;
    }

    public long getNHWAC_FEMALE() {
        return NHWAC_FEMALE;
    }

    public void setNHWAC_FEMALE(long NHWAC_FEMALE) {
        this.NHWAC_FEMALE = NHWAC_FEMALE;
    }

    public long getNHBAC_MALE() {
        return NHBAC_MALE;
    }

    public void setNHBAC_MALE(long NHBAC_MALE) {
        this.NHBAC_MALE = NHBAC_MALE;
    }

    public long getNHBAC_FEMALE() {
        return NHBAC_FEMALE;
    }

    public void setNHBAC_FEMALE(long NHBAC_FEMALE) {
        this.NHBAC_FEMALE = NHBAC_FEMALE;
    }

    public long getNHIAC_MALE() {
        return NHIAC_MALE;
    }

    public void setNHIAC_MALE(long NHIAC_MALE) {
        this.NHIAC_MALE = NHIAC_MALE;
    }

    public long getNHIAC_FEMALE() {
        return NHIAC_FEMALE;
    }

    public void setNHIAC_FEMALE(long NHIAC_FEMALE) {
        this.NHIAC_FEMALE = NHIAC_FEMALE;
    }

    public long getNHAAC_MALE() {
        return NHAAC_MALE;
    }

    public void setNHAAC_MALE(long NHAAC_MALE) {
        this.NHAAC_MALE = NHAAC_MALE;
    }

    public long getNHAAC_FEMALE() {
        return NHAAC_FEMALE;
    }

    public void setNHAAC_FEMALE(long NHAAC_FEMALE) {
        this.NHAAC_FEMALE = NHAAC_FEMALE;
    }

    public long getNHNAC_MALE() {
        return NHNAC_MALE;
    }

    public void setNHNAC_MALE(long NHNAC_MALE) {
        this.NHNAC_MALE = NHNAC_MALE;
    }

    public long getNHNAC_FEMALE() {
        return NHNAC_FEMALE;
    }

    public void setNHNAC_FEMALE(long NHNAC_FEMALE) {
        this.NHNAC_FEMALE = NHNAC_FEMALE;
    }

    public long getH_MALE() {
        return H_MALE;
    }

    public void setH_MALE(long h_MALE) {
        H_MALE = h_MALE;
    }

    public long getH_FEMALE() {
        return H_FEMALE;
    }

    public void setH_FEMALE(long h_FEMALE) {
        H_FEMALE = h_FEMALE;
    }

    public long getHWA_MALE() {
        return HWA_MALE;
    }

    public void setHWA_MALE(long HWA_MALE) {
        this.HWA_MALE = HWA_MALE;
    }

    public long getHWA_FEMALE() {
        return HWA_FEMALE;
    }

    public void setHWA_FEMALE(long HWA_FEMALE) {
        this.HWA_FEMALE = HWA_FEMALE;
    }

    public long getHBA_MALE() {
        return HBA_MALE;
    }

    public void setHBA_MALE(long HBA_MALE) {
        this.HBA_MALE = HBA_MALE;
    }

    public long getHBA_FEMALE() {
        return HBA_FEMALE;
    }

    public void setHBA_FEMALE(long HBA_FEMALE) {
        this.HBA_FEMALE = HBA_FEMALE;
    }

    public long getHIA_MALE() {
        return HIA_MALE;
    }

    public void setHIA_MALE(long HIA_MALE) {
        this.HIA_MALE = HIA_MALE;
    }

    public long getHIA_FEMALE() {
        return HIA_FEMALE;
    }

    public void setHIA_FEMALE(long HIA_FEMALE) {
        this.HIA_FEMALE = HIA_FEMALE;
    }

    public long getHAA_MALE() {
        return HAA_MALE;
    }

    public void setHAA_MALE(long HAA_MALE) {
        this.HAA_MALE = HAA_MALE;
    }

    public long getHAA_FEMALE() {
        return HAA_FEMALE;
    }

    public void setHAA_FEMALE(long HAA_FEMALE) {
        this.HAA_FEMALE = HAA_FEMALE;
    }

    public long getHNA_MALE() {
        return HNA_MALE;
    }

    public void setHNA_MALE(long HNA_MALE) {
        this.HNA_MALE = HNA_MALE;
    }

    public long getHNA_FEMALE() {
        return HNA_FEMALE;
    }

    public void setHNA_FEMALE(long HNA_FEMALE) {
        this.HNA_FEMALE = HNA_FEMALE;
    }

    public long getHTOM_MALE() {
        return HTOM_MALE;
    }

    public void setHTOM_MALE(long HTOM_MALE) {
        this.HTOM_MALE = HTOM_MALE;
    }

    public long getHTOM_FEMALE() {
        return HTOM_FEMALE;
    }

    public void setHTOM_FEMALE(long HTOM_FEMALE) {
        this.HTOM_FEMALE = HTOM_FEMALE;
    }

    public long getHWAC_MALE() {
        return HWAC_MALE;
    }

    public void setHWAC_MALE(long HWAC_MALE) {
        this.HWAC_MALE = HWAC_MALE;
    }

    public long getHWAC_FEMALE() {
        return HWAC_FEMALE;
    }

    public void setHWAC_FEMALE(long HWAC_FEMALE) {
        this.HWAC_FEMALE = HWAC_FEMALE;
    }

    public long getHBAC_MALE() {
        return HBAC_MALE;
    }

    public void setHBAC_MALE(long HBAC_MALE) {
        this.HBAC_MALE = HBAC_MALE;
    }

    public long getHBAC_FEMALE() {
        return HBAC_FEMALE;
    }

    public void setHBAC_FEMALE(long HBAC_FEMALE) {
        this.HBAC_FEMALE = HBAC_FEMALE;
    }

    public long getHIAC_MALE() {
        return HIAC_MALE;
    }

    public void setHIAC_MALE(long HIAC_MALE) {
        this.HIAC_MALE = HIAC_MALE;
    }

    public long getHIAC_FEMALE() {
        return HIAC_FEMALE;
    }

    public void setHIAC_FEMALE(long HIAC_FEMALE) {
        this.HIAC_FEMALE = HIAC_FEMALE;
    }

    public long getHAAC_MALE() {
        return HAAC_MALE;
    }

    public void setHAAC_MALE(long HAAC_MALE) {
        this.HAAC_MALE = HAAC_MALE;
    }

    public long getHAAC_FEMALE() {
        return HAAC_FEMALE;
    }

    public void setHAAC_FEMALE(long HAAC_FEMALE) {
        this.HAAC_FEMALE = HAAC_FEMALE;
    }

    public long getHNAC_MALE() {
        return HNAC_MALE;
    }

    public void setHNAC_MALE(long HNAC_MALE) {
        this.HNAC_MALE = HNAC_MALE;
    }

    public long getHNAC_FEMALE() {
        return HNAC_FEMALE;
    }

    public void setHNAC_FEMALE(long HNAC_FEMALE) {
        this.HNAC_FEMALE = HNAC_FEMALE;
    }



}
